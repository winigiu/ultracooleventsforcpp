#pragma once
#include <windows.h>

typedef bool(*fnTrigger)(void);

template<typename params>
class EventList;
template<typename params>
class Event;

template<typename params>
struct callback
{
	typedef void(*cback)(params);
	const char* name;
	cback callback;
};

template<typename params>
class Event
{
private:
	callback<params>* Callbacks;
	int CallbacksIterator = 0;
	int CallbacksSize = 0;

public:
	const char* name;
	fnTrigger Trigger;

	Event(const char* EventName, fnTrigger trigger, EventList<params>* list)
	{
		name = EventName;
		Trigger = trigger;

		list->registerEvent(this);
	}
	~Event()
	{
		delete Callbacks;
	}

	bool findCallback(const char* name)
	{
		for (int i = 0; i < CallbacksSize; i++)
		{
			callback* cbk;
			if (!strcmp(name, cbk->name))
			{
				CallbacksIterator = i;
				return  true;
			}
		}
		return false;
	}

	void registerCallback(callback<params>* cback)
	{
		if (findCallback(cback->name))
		{
			//throw callback_already_present;
			CallbacksSize++;
			Callbacks[CallbacksIterator] = cback;
			return;
		}
		Callbacks[CallbacksSize + 1] = cback;
	}
	void unregisterCallback(const char* name)
	{
		if (findCallback(name))
		{
			CallbacksSize--;
			Callbacks[CallbacksIterator] = { 0 };
		}
	}
};


template<typename params>
class EventList
{
private:
	Event<params>* list;
	int listIterator = 0;
	int listSize = 0;

	int pollDelay;
	params parStruct;
	HANDLE listen;

	void CallTrigger()
	{
		void* zeroes = malloc(sizeof Event<params>);
		void* zeroesTwo = malloc(sizeof callback<params>);

		for (int i = 0; i < list.size(); i++, Sleep(pollDelay) ) 
		{
			Event* v = list[i];
			if (!memcmp(v, zeroes, sizeof Event<params>))// check if shitty
				if (v->Trigger())
					for (int j = 0; j < v->CallbacksSize; j++)
						if( !memcmp(v->Callbacks[j], zeroesTwo, sizeof callback<params> ) ) // check if shitty
							v->Callbacks[j]->callback(parStruct);

		}
		free(zeroes);
		free(zeroesTwo);
	}
public:	
	EventList(int poll, params paramStruct)
	{
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)CallTrigger, NULL, NULL, &listen);
	}
	~EventList()
	{
		delete list;
		CloseHandle(listen);
	}

	bool findEvent(const char* name)
	{
		for (int i = 0; i < listSize; i++)
		{
			Event* cbk =  list[i];
			if (!strcmp(name, cbk->name))
			{
				listIterator = i;
				return true;
			}
		}
		return false;
	}

	void registerEvent(Event<params>* evnt)
	{
		if (findEvent(evnt->name))
		{
			//throw event_already_present;
			listSize++;
			list[listIterator] = evnt;
			return;
		}
		list[listIterator] = evnt;
	}
	void unregisterEvent(const char* name)
	{
		if (findEvent(name))
		{
			listSize--;
			list[listIterator] = { 0 };
		}
	}
};